import { Component, OnInit } from '@angular/core';
import { Product } from '../../models/product';
import { ProductsService } from 'src/app/services/products.service';
import { EMPTY, Observable, Subject } from 'rxjs';
import {
  map,
  tap,
  debounceTime,
  switchMap,
  distinctUntilChanged,
  filter,
  mergeMap
} from 'rxjs/operators';

@Component({
  selector: 'app-searchbox',
  templateUrl: './searchbox.component.html',
  styleUrls: ['./searchbox.component.scss'],
})
export class SearchboxComponent implements OnInit {
  constructor(private productsService: ProductsService) {}

  ngOnInit(): void {
  }

  newSearch(): void {
  }

  selectItem(item: Product): void {
  }
}

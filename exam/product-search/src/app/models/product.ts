export interface Product {
  productCode: string;
  productName: string;
  attributes: string;
  tags: string;
  productPrice: string;
  salePrice: string;
  photoClonedFrom: string;
}
